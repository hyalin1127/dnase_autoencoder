# DNAse_autoencoder
DNase is a popular assay to assess chromatin accessibility. To embed complicated DNase landscape into low dimension vector, we employ auto-encoder.

# Training data

# Model
![DNase autoencoder model](DNase_autoencoder_model.png)

# Results
![Learned DNAse features](Learned_DNAse_features.png)
